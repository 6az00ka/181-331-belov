import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtMultimedia 5.14

ApplicationWindow {
    visible: true
    width: 600
    height: 800
    title: "Проигрыватель"
    property int track_index:-1
    SwipeView {
        id: swipeView
        anchors.fill: parent
        Page{
            id: page1
            header:
                ToolBar{
                anchors.top: parent.top
                background: Rectangle{
                    implicitHeight: 50
                    width: parent.width
                    color: "#313A46"
                    Image{
                        id:hmg2
                        //source: "media/ifruit5050.png"
                        anchors.right: txt2.left
                        anchors.rightMargin: 6
                        width: 30
                        height: 30
                        anchors.verticalCenter: parent.verticalCenter
                    }
                    Text {
                        id: txt2
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.horizontalCenter: parent.horizontalCenter
                        font.family: "Helvetica"
                        font.pointSize: 16
                        color: "#ffffff"
                        text: "Записи"
                    }
                }
            }
            ListModel{
                id: mod1
                ListElement{
                    isrc: "media/pic1.jpg"
                    aut: "Glitch"
                    son: "Obsidian"
                    path: "C:/Projects/QtExam/181-331_Belov/media/Glitch-Obsidian.mp3"
                }
                ListElement{
                    isrc: "media/pic2.jpg"
                    aut: "Helkimer"
                    son: "Back"
                    path: "C:/Projects/QtExam/181-331_Belov/media/Helkimer-Back.mp3"
                }
                ListElement{
                    isrc: "media/pic3.jpg"
                    aut: "Mixaund"
                    son: "Team"
                    path: "C:/Projects/QtExam/181-331_Belov/media/Mixaund-Team.mp3"
                }
                ListElement{
                    isrc: "media/pic1.jpg"
                    aut: "Punch"
                    son: "Journey"
                    path: "C:/Projects/QtExam/181-331_Belov/media/Punch-Journey.mp3"
                }
                ListElement{
                    isrc: "media/pic2.jpg"
                    aut: "Roa"
                    son: "Relax"
                    path: "C:/Projects/QtExam/181-331_Belov/media/Roa-Relax.mp3"
                }
            }

            ListView
            {
                id: lst1
                width: parent.width*0.9
                height: parent.height*0.9
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                anchors.margins: 10
                spacing: 10
                model: mod1
                delegate: Rectangle{
                    width: parent.width
                    height: 100
                    color: "#D0313A46"
                    Image {
                        id: imgmod
                        source: isrc
                        anchors.left: parent.left
                        anchors.top: parent.top
                        anchors.margins: 5
                        height: parent.height*0.8
                        fillMode: Image.PreserveAspectFit
                    }
                    Text{
                        id: authorm
                        text: aut
                        anchors.margins: 5
                        anchors.left: imgmod.right
                        anchors.top: parent.top
                        color: "white"
                        font.family: "Helvetica"
                        font.pointSize: 12

                    }
                    Text{
                        id: song
                        text: son
                        anchors.margins: 5
                        anchors.left: imgmod.right
                        anchors.top: authorm.bottom
                        color: "white"
                        font.family: "Helvetica"
                        font.pointSize: 12
                    }
                    Button{
                        id: playb
                        height: 40
                        width: height
                        anchors.left: imgmod.right
                        anchors.top: song.bottom
                        anchors.margins: 10
                        background: Rectangle{
                            color: "white"
                            Text{
                                anchors.horizontalCenter: parent.horizontalCenter
                                anchors.verticalCenter: parent.verticalCenter
                                text: "Play"
                                font.family: "Helvetica"
                                font.pointSize: 12
                            }
                        }
                        onClicked:{
                            track_index = index;
                            swipeView.currentIndex=1;
                            img22.source = isrc;
                            play.source = path;
                            t1.color="grey";
                            t2.color="grey";
                            t3.color="grey";
                            t4.color="grey";
                            t5.color="grey";
                            play.play();
                        }
                    }

                }
            }
        }

        Page{
            id: page2
            header:
                ToolBar{
                anchors.top: parent.top
                background: Rectangle{
                    implicitHeight: 50
                    width: parent.width
                    color: "#313A46"
                    Image{
                        id:hmg1
                        //source: "media/ifruit5050.png"
                        anchors.right: txt1.left
                        anchors.rightMargin: 6
                        width: 30
                        height: 30
                        anchors.verticalCenter: parent.verticalCenter
                    }
                    Text {
                        id: txt1
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.horizontalCenter: parent.horizontalCenter
                        font.family: "Helvetica"
                        font.pointSize: 16
                        color: "#ffffff"
                        text: "Воспроизведение"
                    }
                    Button{
                        id: btn61
                        width: 70
                        implicitHeight: 35
                        anchors.top: parent.top
                        anchors.right: parent.right
                        anchors.margins: 5
                        Text{
                            text: "Назад"
                            color: "black"
                            font.pointSize: 10
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                        }
                        background: Rectangle{
                            width: parent.width
                            height: parent.height
                            radius: 5
                            color: "white"
                        }
                        onClicked:
                        {
                            play.stop();
                            swipeView.currentIndex = 0;
                        }
                    }
                }

            }
            Image {
                id: img22
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: parent.top
                anchors.margins: 10
                height: parent.height*0.5
                fillMode: Image.PreserveAspectFit
                MouseArea {
                    width: parent.width
                    height: parent.height
                    preventStealing: true
                    property real velocity: 0.0
                    property int xStart: 0
                    property int xPrev: 0
                    property bool tracing: false
                    onPressed: {
                        xStart = mouse.x
                        xPrev = mouse.x
                        velocity = 0
                        tracing = true
                    }
                    onPositionChanged: {
                        if ( !tracing ) return
                        var currVel = (mouse.x-xPrev)
                        velocity = (velocity + currVel)/2.0
                        xPrev = mouse.x
                        if ( velocity > 15 && mouse.x > parent.width*0.2 ) {
                            tracing = false
                        }
                        else if (velocity < -15 && mouse.x < parent.width*0.2) {
                            tracing = false
                        }
                    }
                    onReleased: {
                        tracing = false
                        if ( velocity > 15 && mouse.x > parent.width*0.2 ) {
                            // SWIPE DETECTED
                            if (track_index===0)
                            {track_index=mod1.count-1;}
                            else{track_index--;}
                            play.stop();
                            img22.source=mod1.get(track_index).isrc;
                            play.source=mod1.get(track_index).path;
                            t1.color="grey";
                            t2.color="grey";
                            t3.color="grey";
                            t4.color="grey";
                            t5.color="grey";
                            play.play();
                        }
                        else if (velocity < -15 && mouse.x < parent.width*0.2) {
                            if(track_index===mod1.count-1){track_index = 0;}
                            else{track_index++;}
                            play.stop();
                            img22.source=mod1.get(track_index).isrc;
                            play.source=mod1.get(track_index).path;
                            t1.color="grey";
                            t2.color="grey";
                            t3.color="grey";
                            t4.color="grey";
                            t5.color="grey";
                            play.play();
                            // SWIPE DETECTED
                        }
                    }
                }
            }
            MediaPlayer {
                id: play
                //source: "C:/Projects/QtExam/181-331_Belov/media/Glitch-Obsidian.mp3"
                onPositionChanged: {
                    sld.sync = true
                    sld.value = play.position
                    sld.sync = false
                }
            }
            RowLayout{
                visible: play.hasAudio
                anchors.bottom: lcurrent.top
                anchors.left: sld.left
                anchors.bottomMargin: 20
                height: 70
                Button{
                    id: r1
                    height: 50
                    width: height
                    Layout.rightMargin: 15
                    background: Rectangle{
                        height:parent.height
                        width:parent.width
                        color: "#00000000"
                        Text{
                            id: t1
                            font.pointSize: 20
                            text: "★"
                            color: "grey"
                        }
                    }
                    onClicked:{
                        t1.color="blue";
                        t2.color="grey";
                        t3.color="grey";
                        t4.color="grey";
                        t5.color="grey";
                    }
                }
                Button{
                    id: r2
                    height: 50
                    width: height
                    Layout.rightMargin: 15
                    background: Rectangle{
                        height:parent.height
                        width:parent.width
                        color: "#00000000"
                        Text{
                            id: t2
                            font.pointSize: 20
                            text: "★"
                            color: "grey"
                        }
                    }
                    onClicked:{
                        t1.color="blue";
                        t2.color="blue";
                        t3.color="grey";
                        t4.color="grey";
                        t5.color="grey";
                    }
                }
                Button{
                    id: r3
                    height: 50
                    width: height
                    Layout.rightMargin: 15
                    background: Rectangle{
                        height:parent.height
                        width:parent.width
                        color: "#00000000"
                        Text{
                            id: t3
                            font.pointSize: 20
                            text: "★"
                            color: "grey"
                        }
                    }
                    onClicked:{
                        t1.color="blue";
                        t2.color="blue";
                        t3.color="blue";
                        t4.color="grey";
                        t5.color="grey";
                    }
                }
                Button{
                    id: r4
                    height: 50
                    width: height
                    Layout.rightMargin: 15
                    background: Rectangle{
                        height:parent.height
                        width:parent.width
                        color: "#00000000"
                        Text{
                            id: t4
                            font.pointSize: 20
                            text: "★"
                            color: "grey"
                        }
                    }
                    onClicked:{
                        t1.color="blue";
                        t2.color="blue";
                        t3.color="blue";
                        t4.color="blue";
                        t5.color="grey";
                    }
                }
                Button{
                    id: r5
                    height: 50
                    width: height
                    Layout.rightMargin: 15
                    background: Rectangle{
                        height:parent.height
                        width:parent.width
                        color: "#00000000"
                        Text{
                            id: t5
                            font.pointSize: 20
                            text: "★"
                            color: "grey"
                        }
                    }
                    onClicked:{
                        t1.color="blue";
                        t2.color="blue";
                        t3.color="blue";
                        t4.color="blue";
                        t5.color="blue";
                    }
                }
            }
            Label{
                id: lcurrent
                visible: play.hasAudio
                anchors.left: sld.left
                anchors.bottom: sld.top
                anchors.margins: 5
                color: "grey"
                font.family: "Helvetica"
                font.pointSize: 12
                readonly property int minutes: Math.floor(play.position / 60000)
                readonly property int seconds: Math.round((play.position % 60000) / 1000)
                text: Qt.formatTime(new Date(0, 0, 0, 0, minutes, seconds), qsTr("mm:ss"))
            }
            Label{
                id: lall
                visible: play.hasAudio
                anchors.right: sld.right
                anchors.bottom: sld.top
                anchors.margins: 5
                color: "grey"
                font.family: "Helvetica"
                font.pointSize: 12
                readonly property int minutes: Math.floor(play.duration / 60000)
                readonly property int seconds: Math.round((play.duration % 60000) / 1000)
                text: Qt.formatTime(new Date(0, 0, 0, 0, minutes, seconds), qsTr("mm:ss"))
            }
            Slider{
                id: sld
                anchors.bottom: btn21.top
                width: parent.width
                to: play.duration //длительность
                property bool sync: false
                onValueChanged: {if(!sync){play.seek(value)}}
            }
            Button{
                id:btn21
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottom: parent.bottom
                anchors.margins: 10
                icon.source: play.playbackState === MediaPlayer.PlayingState ? "media/stop.png" : "media/play.png"
                onClicked: play.playbackState === MediaPlayer.PlayingState ? play.pause() : play.play()
                background: Rectangle {
                    color: "#00000000"
                    radius: 30
                }
            }
            Button{
                id:btn22
                anchors.right: btn21.left
                anchors.bottom: parent.bottom
                anchors.margins: 10
                anchors.top: sld.bottom
                icon.source: "media/prev.png"
                background: Rectangle {
                    color: "#00000000"
                    radius: 30
                }
                onClicked: {
                    if (track_index===0)
                    {track_index=mod1.count-1;}
                    else{track_index--;}
                    play.stop();
                    img22.source=mod1.get(track_index).isrc;
                    play.source=mod1.get(track_index).path;
                    t1.color="grey";
                    t2.color="grey";
                    t3.color="grey";
                    t4.color="grey";
                    t5.color="grey";
                    play.play();
                }
            }
            Button{
                id:btn23
                anchors.left: btn21.right
                anchors.bottom: parent.bottom
                anchors.margins: 10
                anchors.top: sld.bottom
                icon.source: "media/next.png"
                background: Rectangle {
                    color: "#00000000"
                    radius: 30
                }
                onClicked: {
                    if(track_index===mod1.count-1){track_index = 0;}
                    else{track_index++;}
                    play.stop();
                    img22.source=mod1.get(track_index).isrc;
                    play.source=mod1.get(track_index).path;
                    t1.color="grey";
                    t2.color="grey";
                    t3.color="grey";
                    t4.color="grey";
                    t5.color="grey";
                    play.play();
                }
            }
        }
        Drawer{
            id: dr1
            width: parent.width * 0.5
            height: parent.height
            dragMargin: 50
            Rectangle{
                anchors.fill: parent
                color: "#A0FFFFFF"
            }
            Image{
                id: imgpol
                width: parent.width * 0.7
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
                source: "media/polylogo.jpg"
                fillMode: Image.PreserveAspectFit
            }
            Text {
                id: drtxt11
                color: "black"
                text: "Проигрыватель"
                anchors.horizontalCenter: parent.horizontalCenter
                horizontalAlignment: Text.AlignHCenter
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.margins: 10
                width: parent.width*0.9
                font.family: "Helvetica"
                font.pointSize: 16
            }
            Text {
                id: drtxt1
                color: "black"
                text: "Экзаменационное задание по дисциплине \"Разработка безопасных мобильных приложений\". Московский Политех, 30 июня 2020г. "
                anchors.horizontalCenter: parent.horizontalCenter
                horizontalAlignment: Text.AlignHCenter
                anchors.left: parent.left
                anchors.top: drtxt11.bottom
                anchors.margins: 10
                width: parent.width*0.9
                wrapMode: TextArea.WordWrap
                font.family: "Helvetica"
                font.pointSize: 12
            }
            Text {
                id: hyperlink
                color: "black"
                text: "Repo: <a href='https://gitlab.com/6az00ka/181-331-belov'>this link</a>"
                onLinkActivated: Qt.openUrlExternally("https://gitlab.com/6az00ka/181-331-belov")
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                anchors.margins: 10
                horizontalAlignment: Text.AlignRight
                width: parent.width*0.9
                font.family: "Helvetica"
                font.pointSize: 12
            }
            Text{
                id: author
                color: "black"
                text: "Автор: pavel.belov00@mail.ru"
                anchors.right: parent.right
                anchors.bottom: hyperlink.top
                anchors.margins: 10
                horizontalAlignment: Text.AlignRight
                width: parent.width*0.9
                font.family: "Helvetica"
                font.pointSize: 12
            }
        }
    }
}
