# Экзаменационное приложение "Проигрыватель", Вариант 2

## Описание 
### Приложение разрабатывается в рамках экзамена по дисциплине "Разработка безопасных мобильных приложений"
### Автор: pavel.belov00@mail.ru
### https://new.mospolytech.ru/

## Снимки
### <img src="img/1.jpg" height=200> &ensp; <img src="img/2.jpg" height=200> &ensp; <img src="img/3.jpg" height=200> 

## О создании проекта
### Проект разрабатывался в среде разработки Qt Creator на языке QML
### Цель разработки: создание аналога мобильного музыкального проигрывателя
### Причина разработки: сдача очного экзамена World Skills 
### Дата разработки: 30.06.2020